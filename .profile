# exports
export ACRONYMDB="$HOME/.local/share/acronyms"
export BADWOLF_L10N="en_GB:en_US:fr_FR:de_DE:ru_RU"
export BROWSER="badwolf:firefox:lynx"
export EDITOR="vis"
export FCEDIT="${EDITOR:-ed}"
export GOPROXY="direct" # disable proxying to Google by default
export GOSUMDB=off # do not query for module checksum when undefined in go.sum
export GO111MODULE=on
export GOPATH="$HOME/go:/usr/lib/go-gentoo"
export ERL_AFLAGS="-kernel shell_history enabled"
unset GIT_ALTERNATE_OBJECT_DIRECTORIES
unset GIT_OBJECT_DIRECTORY
export GPGKEY='DDC9237C14CF6F4DD847F6B390D93ACCFEFF61AE'
export GTK_CSD=0
export GTK_OVERLAY_SCROLLING=0
export LANG="en_DK.UTF-8"
export LC_ADDRESS='fr_FR.UTF-8'
export LC_TIME='en_DK.UTF-8' # Keep on ISO-8601 even when temporarly setting other LANG
export LC_MEASUREMENT="en_DK.UTF-8" # Same but mesures
export LC_MONETARY="fr_FR.UTF-8"
export LC_NUMERIC="mfe_MU.UTF-8" # decimal_point="." thousands_sep="<U202F>" grouping=3
export LC_TELEPHONE='fr_FR.UTF-8'
export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.pdf=00;32:*.ps=00;32:*.txt=00;32:*.patch=00;32:*.diff=00;32:*.log=00;32:*.tex=00;32:*.doc=00;32:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:'
export MOZ_ALLOW_GTK_DARK_THEME=true
export NNTPSERVER='news.free.fr'
#export PATH="$(echo /{,usr/{,games/,local/,lib/llvm/{5,4}/,lib/plan9/}}{,s}bin | tr \  :)"
export PATH="$HOME/.local/bin:$PATH:$HOME/go/bin"
export PLUG_TMPDIR="/tmp/plug-$(id -u)"
export PS4='[32m$ [0m'
export QEMU_AUDIO_DRV=alsa
export SSH_AGENT_PID=$(cat ${HOME}/.ssh/agent.pid)
export SSH_AUTH_SOCK="${HOME}/.ssh/agent.sock"
export SSH_ASK_PASS="${HOME}/.local/bin/ssh-ask-pass.sh"
export TERMINAL='foot'
export TZ='Europe/Paris'
export QT_STYLE_OVERRIDE=adwaita-dark
export CMAKE_GENERATOR=Ninja

# Own environment variables for agnostism
export MY_WALLPAPER_MODE='fill' # one of fill, center, tile
export MY_WALLPAPER="/tank/datalove/img/wallpaper/background_dual.png"

# Input Method
export XMODIFIERS=@im=uim
export GTK_IM_MODULE="uim"
export QT_IM_MODULE="uim"

# https://devmanual.gentoo.org/general-concepts/manifest/index.html
export PORTAGE_GPG_DIR=$HOME
export PORTAGE_GPG_KEY=$GPGKEY
export FEATURES="sign"

# XDG user directories
export XDG_DESKTOP_DIR="/var/empty"
export XDG_DOCUMENTS_DIR="/var/empty"
export XDG_DOWNLOAD_DIR="$HOME/Downloads"
export XDG_TEMPLATES_DIR="$HOME/templates"
export XDG_VIDEOS_DIR="$HOME/Videos"
export XDG_MUSIC_DIR="$HOME/Music"
export XDG_RUNTIME_DIR="/tmp/$(id -u)-xdg_runtime_dir"
[ -d "${XDG_RUNTIME_DIR}" ] || (
	mkdir -p "${XDG_RUNTIME_DIR}"
	chmod 0700 "${XDG_RUNTIME_DIR}"
)
export XDG_CACHE_HOME="/tmp/$(id -u)-xdg_cache_home"
[ -d "${XDG_CACHE_HOME}" ] || (
	mkdir -p "${XDG_CACHE_HOME}"
	chmod 0700 "${XDG_CACHE_HOME}"
)

## XKB configuration (Wayland+xinit)
#export XKB_DEFAULT_RULES=""
#export XKB_DEFAULT_MODEL=""
export XKB_DEFAULT_LAYOUT="us,ru"
if grep lanodan '/usr/share/X11/xkb/symbols/us' >/dev/null 2>/dev/null
then
	export XKB_DEFAULT_VARIANT="lanodan,phonetic"
else
	export XKB_DEFAULT_VARIANT=",phonetic"
fi
# grp:alt_shift_toggle conflicts with shift:both_capslock
export XKB_DEFAULT_OPTIONS="compose:menu,grp:alt_shift_toggle,terminate:ctrl_alt_bksp,keypad:pointerkeys,ctrl:nocaps,lv3:ralt_switch"

## Fuck off nvidia
export VDPAU_DRIVER=radeonsi
export LIBVA_DRIVER_NAME=radeonsi

## Fuck off GNUisms
export POSIXLY_CORRECT=1
export POSIX_ME_HARDER=1

export WEBKIT_GST_ENABLE_AUDIO_MIXER=1
export WEBKIT_GST_USE_PLAYBIN3=1

gpg-connect-agent /bye # Make sure gpg-agent is started

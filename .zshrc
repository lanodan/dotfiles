setopt prompt_subst
setopt auto_cd
setopt correct_all
setopt bash_auto_list

unsetopt monitor

# Completion
zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' matcher-list '' 'r:|[._-]=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' use-ip true #ssh completion
zstyle ':completion::complete:*' use-cache 1

autoload -U compinit promptinit
compinit
promptinit; prompt gentoo

RPS1=$'%?|%T'

# History
setopt inc_append_history
setopt hist_reduce_blanks
setopt HIST_IGNORE_DUPS

. ~/.common.sh

function command_not_found_handler() {
	fuck
}
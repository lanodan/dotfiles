#!/bin/sh
entry="$(echo "$@" | sed -e 's;^Enter passphrase for '"$HOME/.ssh"';SSH;' -e 's;: $;;')"
pass show "${entry}" | head -1

// Change YouTube iframes to a placeholder containing a link

document.addEventListener("readystatechange", (event) => {
	const elems = document.getElementsByTagName("IFRAME");
	const src_re = new RegExp("^https?://(www\.)?youtube\.com/");
	console.debug(event.target.readyState, `${elems.length} Iframes found`, elems);

	Array.prototype.forEach.call(elems, (elem) => {
		console.debug(`Found iframe`, elem);

		const src = elem.getAttribute("src");

		if(src.match(src_re)) {
			elem.outerHTML = `<div><a href="${src}"><h3>Youtube Frame</h3>${src}</a></div>`;
		}
	});
});

#!/bin/sh
# per-shell exports
export GPG_TTY="$(tty)"
# Aliases
unalias bash; alias bash="SHELL=/bin/bash bash -l"
unalias bc; alias bc="bc ~/Sources/git/hacktivis.me/git/blog/notes/bc.txt"
unalias fuck; alias fuck='TF_ALIAS=fuck eval $(thefuck $(fc -ln -1))'
unalias fuu; alias fuu='su -c "$(fc -ln -1)"'
unalias git-st; alias git-st="git status --short --branch"
#alias git-sel='git checkout $(git branch --format "%(refname:lstrip=2)" | dmenu -i -l 10)'
unalias git-sel; alias git-sel='select branch in $(git branch --format "%(refname:lstrip=2)"); do git checkout $branch; break; done'
unalias git-story; alias git-story="git log --graph --abbrev-commit --decorate --format=format:'%C(cyan)%h%C(reset) %C(green)(%ad)%C(reset) %s %C(red)- %an %C(reset)%C(yellow)%d%C(reset)'"
unalias j; alias j='jobs -l'
unalias ls; alias ls='ls -hFA'
unalias mv; alias mv='mv -i'
unalias pscpu; alias pscpu='ps -eo pid,user,pcpu,pmem,comm --sort -pcpu | head -20'
unalias psmem; alias psmem='ps -eo pid,user,pcpu,pmem,comm --sort -pmem | head -20'
unalias rm; alias rm='rm -ri'
unalias sleep; alias sleep="/bin/sleep" # avoid sleep as a builtin
unalias xstart; alias xstart='exec startx -- $(tty | sed "s/\/dev\/tty/vt/")'
unalias .; alias .='cd .'
unalias ..; alias ..='cd ..'
unalias ...; alias ...='cd ../..'
unalias zzz; alias zzz='locker & (sleep 1 ; memsys)'
unalias ytcp; alias ytcp='yt-dlp --sub-langs all,-live_chat --embed-subs --embed-thumbnail --add-metadata --embed-chapters --compat-options filename --concurrent-fragments 10'
unalias ytcp-autosubs; alias ytcp-autosubs='yt-dlp --sub-langs all,-live_chat --write-subs --write-auto-subs --embed-subs --embed-thumbnail --add-metadata --embed-chapters --compat-options filename --concurrent-fragments 10'
unalias deposix; alias deposix='unset POSIXLY_CORRECT POSIX_ME_HARDER'
# List all possible readings; Convert all to romaji
unalias kks; alias kks='kakasi -p -Ha -Ka -Ja -Ea -ka -i utf8 -o utf8'
# because the latin alphabet is a mess: https://lojban.org/publications/cll/cll_v1.1_xhtml-chapter-chunks/chapter-phonology.html#section-oddball-orthographies
# Character-based: Plan9(Port) tr(1); GNU sed(1) y-command
# Byte-based: POSIX, GNU, BusyBox, … tr(1); BusyBox sed(1) y-command
unalias lojban_cyrillic; alias lojban_cyrillic='9 tr "abcdefgijklmnoprstuvxyzABCDEFGIJKLMNOPRSTUVXYZ" \
                            "абшдефгижклмнопрстувхъзАБШДЕФГИЖКЛМНОПРСТУВХЪЗ"'
unalias cyrillic_lojban; alias cyrillic_lojban='9 tr "абшдефгижклмнопрстувхъзАБШДЕФГИЖКЛМНОПРСТУВХЪЗ" \
                            "abcdefgijklmnoprstuvxyzABCDEFGIJKLMNOPRSTUVXYZ"'
unalias cmd; alias cmd=command
unalias wol-cutievault; alias wol-cutievault="wol 74:d4:35:b0:15:3e"
# = functions =
# Note: POSIX only allows [0-9a-z_A-Z] in function names

get_git_branch() {
	if branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null); then
		case "$branch" in
			"HEAD")
			branch='detached*'
			;;
			"$(hostname)")
			branch='@.'
			;;
		esac

		if git notes list HEAD >/dev/null 2>&1
		then
			notes="🖉"
		else
			notes=""
		fi

		printf -- '[41;36m[30m %s%s[0;31m' "$notes" "$branch"
	else
		echo '[0;36m'
	fi

	unset branch
}

get_working_directory() {
	pwd | sed "s|^$HOME|~|"
}

# == prompt setup ==
case $(hostname) in
	NightmareMoon) host_ansi="33" ;;
	hp2125) host_ansi="37" ;;
	*) host_ansi="31" ;;
esac

jobs_pp() {
	job_count="$(jobs | wc -l)"

	if [ "${job_count}" != "0" ]; then
		printf '%s' "[30;46m${job_count}⏾[47;36m"
	fi
}

case $SHELL in
	*ksh*)
		# hack derived from ksh88(1), works in mksh, doesn't works in bash
		# shellcheck disable=SC2025
		export PS1='$(jobs_pp)[47;30m$?[37;100m[100;30m$(date +w%wT%T)[40;90m[40;32m${USER}[${host_ansi}m@$(hostname)[30;46m$(get_working_directory)$(get_git_branch)[0m $(meep $?)'
	;;
	*bash*)
		export     PS1='\[\e[47;30m\]$(jobs_pp)$?\[\e[37;100m\]\[\e[100;30m\]$(date +w%wT%T)\[\e[40;90m\]\[\e[40;32m\]\u\[\e[${host_ansi}m\]@\h\]\[\e[30;46m\]$(get_working_directory)\[\e[0;36m\]\[\e[0m\] $(meep $?)'
	;;
	*)
		# Don't even try escape codes, and so no powerline-style
		export PS1='$? $(date +w%wT%T) $USER@${HOSTNAME:=$(hostname)} $(get_working_directory) > $(meep $?)'
	;;
esac

# = helpers =
ssh_add_all() {
	# Make ssh-add call ${SSH_ASK_PASS} even outside of X11
	export DISPLAY="${DISPLAY:-dummy}"

	ssh-add $(find ~/.ssh -name 'id_*' -a \( \! -name '*.pub' \))
}

ssh_start_agent() {
	code="$(ssh-add -l; echo $?)"
	
	case "${code}" in
		0) echo 'Managed to list keys, SSH agent appear to be active' ;;
		1) echo 'Failed to list keys' ;;
		2)
			rm -fr "$SSH_AUTH_SOCK"
			eval $(ssh-agent -a "$SSH_AUTH_SOCK")
			echo "$SSH_AGENT_PID" > ~/.ssh/agent.pid
			return 0
		;;
		*) echo "Unknown return code: ${code}" ;;
	esac

	return 1
}

ssh_tunnel() {
	port=${1:-443}
	server=${2:-hacktivis.me}
	target_port=${3:-22$port}
	target_host=${4:-localhost}

	echo "ssh -NL ${target_port}:${target_host}:${port} $server"
	ssh -NL "${target_port}:${target_host}:${port}" "$server"
}

xdg_desktop_menu() {
	echo "$1 $2 $3"
	if [ "$1" = 'install' ]; then
		cp "$2" "${XDG_DESKTOP_DIR:-~/Desktop}"
	fi
}

ssh_socks() {
	server=${1:-hacktivis.me}
	port=${2:-22443}

	echo "ssh -ND $port $server"
	ssh -ND "$port" "$server"
}

pmounts() {
	awk 'BEGIN { print "Mountpoint Type Device Options" }  { print $2 " " $3 " " $1 " " $4 | "sort"}' /proc/mounts | column -t -s' '
}

ucd_grep() {
	grep "$1" /usr/share/unicode-data/UnicodeData.txt | awk -F\; '{ system("unicode -t "$1); print FS "U+"$1 FS $2 }' | column -s\; -t
}
ucd_chars() {
	unicode -n "$*" | tr 'abcdef' 'ABCDEF' | while read -r char; do ucd_code "$char"; done
}
ucd_code() {
	awk -F\; '{ if($1 == "'"${1}"'"){print "U+"$1 FS $2} }' /usr/share/unicode-data/UnicodeData.txt | grep . || echo "$1"
}

log_cmd() {
	echo "$(date -u '+%FT%TZ')	START	$*" >> "${HOME}/.log_cmd_${1}.log"
	"$@"
	echo "$(date -u '+%FT%TZ')	EXITED[$?]	$*" >> "${HOME}/.log_cmd_${1}.log"
}

# Wayland wrapper
wstart() {
	[ -z "${XDG_RUNTIME_DIR}" ] && ( echo "$0"': Error: XDG_RUNTIME_DIR undefined, exiting…' 1>&2; false )

	export GDK_BACKEND=wayland
	export QT_QPA_PLATFORM=wayland-egl
	export CLUTTER_BACKEND=wayland
	export SDL_VIDEODRIVER=wayland
	export MY_LOCKER=swaylock
	export TERMINAL=foot

	# Default to sway when unspecified
	compositor=${1:-sway}

	name="$(echo "$compositor" | sed 's/[^a-z]//g')"
	uptime="$(cut -d' ' -f1 /proc/uptime)"

	echo 'wstart_'"$uptime"'_'"$name"'.log' >&2

	"${@:-sway}" 2>&1 | tee "${XDG_RUNTIME_DIR}"'/wstart_'"$uptime"'_'"$name"'.log' 2>&1

	# Quit the login shell for security
	exit
}

meep() {
	if [ "$1" = "0" ]
	then
		beep -l 50 -f 250
	else
		beep -l 50 -f 150
	fi
}

mkcd() {
	mkdir -p "$1"
	cd "$1" || return
}

h() {
	mkcd "${HOME}/tmp/$(date +%Y-%m)"
}

ver_bump() {
	if [ $# -ne 2 ]; then
		echo "usage: $0 <source ebuild path> <destination ebuild path>"
		return
	fi

	cp -v "${1}" "${2}"
	git add "${2}"
	ekeyword ~all "${2}"
	ebuild "${2}" manifest
	git add "${2}/../Manifest"
}

# Unicode-aware variant inspired by https://qntm.org/files/rot13/rot13.html
rot13() {
	uconv -f utf-8 -t utf-8 -x '::nfd;' \
		| tr \
			'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' \
			'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm'
}

gitclone() {
	url="$1"
	dir=$(echo "$1" | sed -E -e 's;^(https?://|git:);'$HOME'/Sources/git/;' -e 's;.git$;;')

	test -e "$dir" || git clone "$1" "$dir"

	cd "$dir"
}

# https://team-cymru.com/community-services/ip-asn-mapping/
ip2asn_stdin() {
	( echo begin ; cat - ; echo end ) | nc whois.cymru.com 43 | sort -n
}

ip2asn_arg() {
	echo "$@" | nc whois.cymru.com 43
}

if ! command -v finger
then
	finger() {
		echo "${1//@*}" | dial -e "tcp!${1//*@}!finger"
	}
fi

# startup

#stty sane # I can haz urandom
stty -ixon # Disable stop (^S) / start (^Q)
stty -ixoff # Disable sending of start/stop characters

h

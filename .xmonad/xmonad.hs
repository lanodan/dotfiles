import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers -- transience

import XMonad.Actions.PhysicalScreens -- horizontalScreenOrderer
import XMonad.Util.WorkspaceCompare -- getSortByXineramaPhysicalRule
import XMonad.Util.EZConfig         -- additionalKeys

import XMonad.Prompt
import XMonad.Prompt.Window  -- windowPrompt

import XMonad.StackSet (focusDown)

import XMonad.Layout.Grid

myLayout = avoidStruts (Full ||| tiled ||| Mirror tiled ||| Grid )
	where
		tiled   = Tall nmaster delta ratio
		nmaster = 1
		ratio   = 1/2
		delta   = 3/100

myManageHook = composeOne
	[ transience
	, role =? "browser" -?> doShift "2"
	, isDialog -?> doFloat
	, className =? "st-256color" -?> doShift "1"
	, className =? "Navigator" -?> doShift "2"
	, className =? "MiniBrowser" -?> doShift "2"
	, className =? "ffplay" -?> doShift "3"
	, className =? "mpv" -?> doShift "3"
	, className =? "Pidgin" -?> doF focusDown
	]
	where
		role = stringProperty "WM_WINDOW_ROLE"

promptConfig = defaultXPConfig
  { font        = "xft:monospace-10"
  , borderColor = "#1e2320"
  , fgColor     = "#dddddd"
  , bgColor     = "#1e2320"
  , fgHLight    = "#ffffff"
  , bgHLight    = "#5f5f5f"
  , height      = 16
  , position    = Top
  }

main = xmonad $ docks $ def
	{ borderWidth        = 0
	, focusFollowsMouse  = False
	, clickJustFocuses   = False
	, logHook            = dynamicLogString xmobarPP
		{ ppTitle   = xmobarColor "green" ""
		, ppVisible = wrap "[" "]"
		, ppSort    = getSortByXineramaPhysicalRule horizontalScreenOrderer
		} >>= xmonadPropLog
	, modMask            = mod4Mask
	, manageHook         = manageHook def <+> myManageHook
	, layoutHook         = myLayout
	, handleEventHook    = handleEventHook def <+> docksEventHook
	, terminal           = "st"
	}
	`additionalKeys`
	[ ((mod4Mask,   xK_b), sendMessage ToggleStruts)
	, ((mod4Mask .|. shiftMask, xK_g     ), windowPrompt promptConfig { autoComplete = Just 500000 } Goto allWindows)
	, ((mod4Mask .|. shiftMask, xK_b     ), windowPrompt promptConfig Bring allWindows)
	]

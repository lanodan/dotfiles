"set runtimepath=/seagate/sources/git/github.com/Shougo/vimproc,~/.vim,/usr/share/vim/vimfiles
let g:neocomplete#enable_at_startup = 1

filetype off "KISS and no PEP8
filetype plugin on
filetype plugin indent off "KISS and no PEP8

set textwidth=0
set nobomb
set encoding=utf-8

set noautoindent
set nosmartindent
set hlsearch

set tabpagemax=16
set foldmethod=indent
set tabstop=4

syntax on
colorscheme solarized
set bg=dark

set list
set rnu

set statusline=%<%F\ [%{&ff}][%{&enc}%{((exists(\"+bomb\")\ &&\ &bomb)?\",\ BOM\":\"\")}]\ %=\ %-14.(%l,%c%V%)\ %P
set laststatus=2
" app-vim/vim-latex
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'

if !exists('g:neocomplete#sources#omni#input_patterns')
	let g:neocomplete#sources#omni#input_patterns = {}
	let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
	let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
endif

-- Copyright © 2017 morhetz <morhetz@gmail.com>
-- Copyright © 2023 Haelwenn (lanodan) Monnier <contact+gruvbox.lua@hacktivis.me>
-- SPDX-License-Identifier: MIT
-- Port of gruvbox https://github.com/morhetz/gruvbox for vis

local lexers = vis.lexers

gb = {}

gb.dark0_hard  = '#1d2021'     -- 29-32-33
gb.dark0       = '#282828'     -- 40-40-40
gb.dark0_soft  = '#32302f'     -- 50-48-47
gb.dark1       = '#3c3836'     -- 60-56-54
gb.dark2       = '#504945'     -- 80-73-69
gb.dark3       = '#665c54'     -- 102-92-84
gb.dark4       = '#7c6f64'     -- 124-111-100
gb.dark4_256   = '#7c6f64'     -- 124-111-100

gb.gray_245    = '#928374'     -- 146-131-116
gb.gray_244    = '#928374'     -- 146-131-116

gb.light0_hard = '#f9f5d7'     -- 249-245-215
gb.light0      = '#fbf1c7'     -- 253-244-193
gb.light0_soft = '#f2e5bc'     -- 242-229-188
gb.light1      = '#ebdbb2'     -- 235-219-178
gb.light2      = '#d5c4a1'     -- 213-196-161
gb.light3      = '#bdae93'     -- 189-174-147
gb.light4      = '#a89984'     -- 168-153-132
gb.light4_256  = '#a89984'     -- 168-153-132

gb.bright_red     = '#fb4934'     -- 251-73-52
gb.bright_green   = '#b8bb26'     -- 184-187-38
gb.bright_yellow  = '#fabd2f'     -- 250-189-47
gb.bright_blue    = '#83a598'     -- 131-165-152
gb.bright_purple  = '#d3869b'     -- 211-134-155
gb.bright_aqua    = '#8ec07c'     -- 142-192-124
gb.bright_orange  = '#fe8019'     -- 254-128-25

gb.neutral_red    = '#cc241d'     -- 204-36-29
gb.neutral_green  = '#98971a'     -- 152-151-26
gb.neutral_yellow = '#d79921'     -- 215-153-33
gb.neutral_blue   = '#458588'     -- 69-133-136
gb.neutral_purple = '#b16286'     -- 177-98-134
gb.neutral_aqua   = '#689d6a'     -- 104-157-106
gb.neutral_orange = '#d65d0e'     -- 214-93-14

gb.faded_red      = '#9d0006'     -- 157-0-6
gb.faded_green    = '#79740e'     -- 121-116-14
gb.faded_yellow   = '#b57614'     -- 181-118-20
gb.faded_blue     = '#076678'     -- 7-102-120
gb.faded_purple   = '#8f3f71'     -- 143-63-113
gb.faded_aqua     = '#427b58'     -- 66-123-88
gb.faded_orange   = '#af3a03'     -- 175-58-3

if is_dark == undefined then
is_dark = true
end

if is_dark then
	gb.bg0 = gb.dark0
	if contrast_dark == 'soft' then
		gb.bg0 = gb.dark0_soft
	elseif contrast_dark == 'hard' then
		gb.bg0 = gb.dark0_hard
	end

	gb.bg1 = gb.dark1
	gb.bg2 = gb.dark2
	gb.bg3 = gb.dark3
	gb.bg4 = gb.dark4

	gb.gray = gb.gray_245

	gb.fg0 = gb.light0
	gb.fg1 = gb.light1
	gb.fg2 = gb.light2
	gb.fg3 = gb.light3
	gb.fg4 = gb.light4

	gb.fg4_256 = gb.light4_256

	gb.red = gb.bright_red
	gb.green = gb.bright_green
	gb.yellow = gb.bright_yellow
	gb.blue = gb.bright_blue
	gb.purple = gb.bright_purple
	gb.aqua = gb.bright_aqua
	gb.orange = gb.bright_orange
else
	gb.bg0 = gb.light0
	if contrast_light == 'soft' then
		gb.bg0 = gb.light0_soft
	elseif contrast_light == 'hard' then
		gb.bg0 = gb.light0_hard
	end

	gb.bg1 = gb.light1
	gb.bg2 = gb.light2
	gb.bg3 = gb.light3
	gb.bg4 = gb.light4

	gb.gray = gb.gray_244

	gb.fg0 = gb.dark0
	gb.fg1 = gb.dark1
	gb.fg2 = gb.dark2
	gb.fg3 = gb.dark3
	gb.fg4 = gb.dark4

	gb.fg4_256 = gb.dark4_256

	gb.red = gb.faded_red
	gb.green = gb.faded_green
	gb.yellow = gb.faded_yellow
	gb.blue = gb.faded_blue
	gb.purple = gb.faded_purple
	gb.aqua = gb.faded_aqua
	gb.orange = gb.faded_orange
end

local italicize_comments = ',notitalics'
if gruvbox_italicize_comments == undefined then
	italicize_comments = ',italics'
else
	if gruvbox_italicize_comments then
		italicize_comments = ',italics'
	end
end

local italicize_strings = ',notitalics'
if gruvbox_italicize_strings == undefined then
	italicize_strings = ',notitalics'
else
	if gruvbox_italicize_strings then
		italicize_strings = ',italics'
	end
end

lexers.STYLE_NOTHING = ''

-- Normal text
lexers.STYLE_DEFAULT = 'fore:'..gb.fg1..',back:'..gb.bg0

-- CursorLine
lexers.STYLE_CURSOR_LINE = 'back:'..gb.bg1
-- miss: CursorColumn

-- miss: TabLineFill, TabLineSel, TabLine
-- miss: MatchParen

-- ColorColumn
lexers.STYLE_COLOR_COLUMN = 'back:'..gb.bg1

-- miss: Conceal
-- miss: CursorLineNr

-- miss: StatusLine
-- miss: VertSplit

-- Folded
lexers.STYLE_FOLDDISPLAYTEXT = 'fore:'..gb.gray..',back:'..gb.bg1..',italics'
-- miss: FoldColumn

-- Cursor
lexers.STYLE_CURSOR = 'back:'..gb.bg2 -- Note: vis lexer doesn't have reverse-video
lexers.STYLE_CURSOR_PRIMARY = 'back:'..gb.gray -- Note: vis lexer doesn't have reverse-video
-- vCursor
lexers.STYLE_SELECTION = 'back:'..gb.bg1
-- miss: iCursor, lCursor

-- miss: Special

-- Comment
lexers.STYLE_COMMENT = 'fore:'..gb.gray..italicize_comments
-- miss: Todo
-- Error
lexers.STYLE_ERROR = 'back:'..gb.red,',bold' -- Note: vim gruvbox uses reverse-video

-- miss: Statement, Conditional, Repeat, Label, Exception
-- Operator
lexers.STYLE_OPERATOR = lexers.STYLE_DEFAULT
lexers.STYLE_KEYWORD = 'fore:'..gb.red

-- Identifier
lexers.STYLE_VARIABLE = 'fore:'..gb.blue
-- Function
lexers.STYLE_FUNCTION = 'fore:'..gb.green..',bold'

-- PreProc
lexers.STYLE_PREPROCESSOR = 'fore:'..gb.aqua
-- miss: Include, Define, Macro, PreCondit

-- Constant
lexers.STYLE_CONSTANT = 'fore:'..gb.purple
-- miss: Character
-- String
if gruvbox_improved_strings == 0 then
	lexers.STYLE_STRING = 'fore:'..gb.green..italicize_strings
else
	lexers.STYLE_STRING = 'fore:'..gb.fg1..',back:'..gb.bg1..italicize_strings
end
-- miss: Boolean
-- Number
lexers.STYLE_NUMBER = 'fore:'..gb.purple
-- miss: Float
-- Type
lexers.STYLE_TYPE = 'fore:'..gb.yellow
-- miss: StorageClass, Structure, Typedef
-- miss: "Completion Menu", "Diffs", "spell"
-- miss: IndentGuidesOdd, IndentGuidesEven

-- Reused: javascriptClassName
lexers.STYLE_CLASS = 'fore:'..gb.yellow

-- Reused: clojureRegexp
lexers.STYLE_REGEX = 'fore:'..gb.aqua

----------------------
-- undefined styles --
----------------------
lexers.STYLE_LABEL = 'fore:'..gb.green
lexers.STYLE_WHITESPACE = ''
lexers.STYLE_EMBEDDED = 'back:'..gb.blue..',bold'
lexers.STYLE_IDENTIFIER = 'fore:'..gb.fg0
lexers.STYLE_LINENUMBER = 'back:'..gb.bg1
lexers.STYLE_LINENUMBER_CURSOR = lexers.STYLE_LINENUMBER .. 'fore:'..gb.yellow
lexers.STYLE_BRACELIGHT = 'fore:'..gb.fg1
lexers.STYLE_BRACEBAD = lexers.STYLE_BRACELIGHT .. lexers.STYLE_ERROR
-- lexers.STYLE_CONTROLCHAR
-- lexers.STYLE_INDENTGUIDE
-- lexers.STYLE_CALLTIP

---------------
-- overrides --
---------------
if gruvbox_comment_hard then
	lexers.STYLE_COMMENT = 'fore:'..gb.aqua..',italics'
end

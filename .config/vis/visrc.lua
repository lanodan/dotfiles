require("vis")

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	vis:command('set rnu')
	vis:command('set show-eof off')
	vis:command('set show-tabs')
	vis:command('set show-newline')
	vis:command('set autoindent')
	vis:command('set theme gruvbox')
	vis:command('set tabwidth 4')
	vis:command('set colorcolumn 80')
end)

-- theme configuration
is_dark = true
--contrast_dark = 'hard'
--contrast_light = 'hard'
gruvbox_comment_hard = true

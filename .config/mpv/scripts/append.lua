-- Copyright © 2023 Haelwenn (lanodan) Monnier <contact+dotfiles@hacktivis.me>
-- SPDX-License-Identifier: MIT

local utils = require 'mp.utils'
local msg = require 'mp.msg'

function get_clipboard(primary)
	local args = {'wl-paste'}
	local res = utils.subprocess({ args = args, cancellable = false })

	if not res.error and res.status == 0 then
		return res.stdout
	else
		msg.error("Error grabbing clipboard:")
		msg.error("  Status: "..(res.status or ""))
		msg.error("  Error: "..(res.error or ""))
		msg.error("  stdout: "..(res.stdout or ""))
		msg.error("args: "..utils.to_string(args))
		return nil
	end
end

function append()
	local clipboard = get_clipboard()
	if clipboard then
		mp.commandv("loadfile", clipboard, "append-play")
		mp.osd_message("append-play: "..clipboard)
		msg.info("append-play: "..clipboard)
	end
end

mp.add_key_binding("ctrl+v", "append", append)

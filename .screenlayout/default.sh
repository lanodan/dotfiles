#!/bin/sh
# Fake xrandr call to reset the default
#xrandr --output DVI-D-1 --auto --pos 0x0

xrandr \
	--output DVI-D-0 --auto --rotate normal --pos 0x0 --primary \
	--output HDMI-A-0 --auto --rotate normal --right-of DVI-D-0
